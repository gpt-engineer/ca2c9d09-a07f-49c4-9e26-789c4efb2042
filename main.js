document.getElementById('search').addEventListener('keyup', function(event) {
  if (event.key === 'Enter') {
    const searchTerm = event.target.value;
    fetch(`https://api.example.com/stocks?search=${searchTerm}`)
      .then(response => response.json())
      .then(data => {
        const stocksSection = document.getElementById('stocks');
        stocksSection.innerHTML = '';
        data.forEach(stock => {
          const card = document.createElement('div');
          card.classList.add('stock-card');
          card.innerHTML = `
            <h2 class="text-xl">${stock.companyName}</h2>
            <p><strong>Symbol:</strong> ${stock.symbol}</p>
            <p><strong>Last Price:</strong> ${stock.lastPrice}</p>
            <p><strong>Change:</strong> ${stock.change}</p>
            <p><strong>Change %:</strong> ${stock.changePercent}</p>
          `;
          card.addEventListener('click', function() {
            const stockInfo = document.getElementById('stock-info');
            stockInfo.innerHTML = `
              <h2 class="text-2xl mb-5">${stock.companyName}</h2>
              <p><strong>Symbol:</strong> ${stock.symbol}</p>
              <p><strong>Last Price:</strong> ${stock.lastPrice}</p>
              <p><strong>Change:</strong> ${stock.change}</p>
              <p><strong>Change %:</strong> ${stock.changePercent}</p>
              <!-- Add more stock info here -->
            `;
          });
          stocksSection.appendChild(card);
        });
      })
      .catch(error => console.error('Error:', error));
  }
});
